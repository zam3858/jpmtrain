<?php

use App\Http\Controllers\AllUserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BahagianController;
use \Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::middleware('auth')
    ->group(function() {
    Route::get('users/update_password', [UserController::class, 'updatePasswordForm'])
        ->name('users.update_password');
    Route::put('users/update_password', [UserController::class, 'updatePasswordSubmit'])
        ->name('users.update_password_submit');

    Route::get('/publicusers/{id}', [UserController::class, 'exportToPdf']);

    Route::resource('/publicusers', UserController::class)->only(['index','show']);

    Route::prefix('admin')
        ->group(function() {
        Route::resource('/users', UserController::class);
        Route::resource('/bahagian', BahagianController::class);
    });
});

Route::get('/get-all-user', AllUserController::class);

//http://booking.test/test
Route::get('/test', function() {
    echo "HELLO, WORLD";
});

//http://booking.test/test/1/apa2
//http://booking.test/test/1
Route::get('/test/{a}/{name?}', function ($id, $name = null)
{
   echo "Hello, " . $id . $name ;
});

//http://booking.test/clash/kereta
Route::get('/clash/kereta', function() {
    echo "Masuk Situ Kereta" ;
});

//http://booking.test/clash/1
Route::get('/clash/{id}', function($id) {
    echo "Masuk Sini " . $id;
});

Route::get('/rawsql', function() {
    $users = DB::select("SELECT * FROM users WHERE id=? OR id=?", [1,400]);
    return $users;
});


