<?php

namespace Database\Seeders;

use App\Models\Bahagian;
use Illuminate\Database\Seeder;

class BahagianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=BahagianSeeder
     * @return void
     */
    public function run()
    {
        Bahagian::create([
            'kod' => 'BPM',
            'nama' => 'BPM',
        ]);

        Bahagian::create([
            'kod' => 'ICU',
            'nama' => 'ICU',
        ]);

    }
}
