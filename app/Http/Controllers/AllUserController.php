<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AllUserController extends Controller
{
    /**
     * @return mixed
     */
    public function __invoke()
    {
        return User::get();
    }
}
