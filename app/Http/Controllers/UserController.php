<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bahagian;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Senaraikan semua pengguna
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dapatkan value search
        $search = request()->search;
        $search2 = request()->search2;

        // dapatkan data users dari table user
        // tarik sekali relationship bahagians dengan eager loading
//        $users_query = User::with('bahagians');
//
//        if (!empty($search)) {
//            $users_query->where('name', $search)
//                ->OrWhere('email', $search)
//                ;
//        }

        $users = User::with(['bahagians' => function($query) use ($search2) {
                            $query->when($search2, function($query) use ($search2) {
                                $query->where('kod', 'LIKE', "%" . $search2 . "%")
                                    ->OrWhere('nama', 'LIKE', "%" . $search2 . "%")
                                ;
                            });
                            }, 'bahagian']
                    )
                    ->when($search, function($query) use ($search) {
                            $query->where('name', 'LIKE', "%" . $search . "%")
                                    ->OrWhere('email', 'LIKE', "%" . $search . "%")
                                    ->orWhereHas('bahagian', function($query) use ($search) {
                                        $query->where('kod', 'LIKE', "%" . $search . "%")
                                            ->OrWhere('nama', 'LIKE', "%" . $search . "%")
                                        ;
                                    })
                                    ->orWhereHas('bahagians', function($query) use ($search) {
                                        $query->where('kod', 'LIKE', "%" . $search . "%")
                                            ->OrWhere('nama', 'LIKE', "%" . $search . "%")
                                        ;
                                    })
                            ;
                    })
//                    ->whereDay('created_at', '28')
//                    ->whereMonth('created_at', '9')
//                    ->whereYear('created_at', '2021')
//                    ->whereDate('created_at', '2021-09-28')
                    ->paginate(20);

        // SELECT *
        // FROM users
        // WHERE name LIKE '%$search5'
        // OR email LIKE '%$search%'
        // OR bahagian_id IN (
        //         SELECT id FROM bahagian WHERE  where kod LIKE '%$search5'
        //         OR nama LIKE '%$search%'
        //  )
        // paparkan data tersebut
        return view('user.index', compact('users','search', 'search2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // paparkan form
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate submitted data
        $request->validate([
            'name' => 'required|max:255|min:2',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
        ]);

        // dapatkan data & prepare rekod baru
        $user = new User();

        $user->name = request()->name;
        $user->email = request()->email;
        $user->password = Hash::make(request()->password);

        // save rekod baru
        $user->save();

        // notification save is success

        // redirect page ke listing (index)
        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dapatkan rekod mengikut ID yang diberi
        $user = User::with('bahagians')->findOrFail($id);

        // paparkan rekod
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $senarai_bahagian = Bahagian::orderBy('nama')->get();
        // dapatkan senarai users untuk dijadikan supervisor
        $users = User::orderBy('name')
                    //->where('id', '!=', $user->id)
                    ->get();

         // paparkan form
        return view('user.edit', [
                'user' => $user,
                'senarai_supervisor' => $users,
                'senarai_bahagian' => $senarai_bahagian
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dapatkan data yang nak dikemaskini
        $user = User::where('id', $id)->firstOrFail();

        // validate data yang disubmit
        $request->validate([
            'name' => ['required', 'max:255', 'min:2'],
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ]
        ]);

        // kemaskini rekod menggunakan data yg disubmit
        $user->name = request()->name;
        $user->email = request()->email;
        $user->bahagian_id  = request()->bahagian_id;

        // save ke database
        $user->save();

        // notification success

        // redirect ke page users
        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dapatkan rekod user yg hendak dibuang
        $user = User::where('id', $id)->firstOrFail();

        // buang rekod
        $user->delete();

        // notifikasi berjaya

        // redirect ke listing
        return redirect(route('users.index'));
    }

    /**
     * display form untuk update password
     */
    public function updatePasswordForm()
    {
        return view('user.update_password');
    }

    public function updatePasswordSubmit()
    {
        // validate
        request()->validate([
            'password' => 'required|confirmed',
        ]);
        // get user untuk dikemaskini (current user yg logged in)
        $user = auth()->user();
        //kemaskini rekod
        $user->password = Hash::make(request()->password);
        $user->save();
        //redirect ke index
        return redirect(route('users.index'));
    }

    public function exportToPdf($id) {
        $user = User::with('bahagians')->findOrFail($id);

        $pdf = \PDF::loadView('user.pdfexport', compact('user'));
        return $pdf->stream();
    }
}
