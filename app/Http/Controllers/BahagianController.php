<?php

namespace App\Http\Controllers;

use App\Models\Bahagian;
use Illuminate\Http\Request;

class BahagianController extends Controller
{
    /**
     * Senaraikan semua pengguna
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dapatkan data bahagians dari table bahagian
        $bahagians = Bahagian::get();

        // paparkan data tersebut
        return view('bahagian.index', compact('bahagians'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // paparkan form
        return view('bahagian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate submitted data
        $request->validate([
            'nama' => 'required|max:255|min:2',
        ]);

        // dapatkan data & prepare rekod baru
        // mass assignment demo
        Bahagian::create(request()->all());

        // notification save is success

        // redirect page ke listing (index)
        return redirect(route('bahagian.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dapatkan rekod mengikut ID yang diberi
        $bahagian = Bahagian::findOrFail($id);

        // paparkan rekod
        return view('bahagian.show', compact('bahagian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Bahagian  $bahagian
     * @return \Illuminate\Http\Response
     */
    public function edit(Bahagian $bahagian)
    {
        $senarai_jabatan = [
            'A1', 'A2', 'BPM', 'C1'
        ];
        // dapatkan senarai bahagians untuk dijadikan supervisor
        $bahagians = Bahagian::orderBy('nama')
            //->where('id', '!=', $bahagian->id)
            ->get();

        // paparkan form
        return view('bahagian.edit', [
            'bahagian' => $bahagian,
            'senarai_supervisor' => $bahagians,
            'senarai_jabatan' => $senarai_jabatan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dapatkan data yang nak dikemaskini
        $bahagian = Bahagian::where('id', $id)->firstOrFail();

        // validate data yang disubmit
        $request->validate([
            'nama' => 'required|max:255|min:2',
        ]);

        // kemaskini rekod menggunakan data yg disubmit
        $bahagian->kod = request()->kod;
        $bahagian->nama = request()->nama;

        // save ke database
        $bahagian->save();

        // notification success

        // redirect ke page bahagians
        return redirect(route('bahagian.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dapatkan rekod bahagian yg hendak dibuang
        $bahagian = Bahagian::where('id', $id)->firstOrFail();

        // buang rekod
        $bahagian->delete();

        // notifikasi berjaya

        // redirect ke listing
        return redirect(route('bahagian.index'));
    }
}
