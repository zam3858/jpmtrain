<x-app-layout>
    <x-slot name="header">
        <span class="header-brand mb-0 h3">{{ __('Bahagian') }}</span>
    </x-slot>

    <div>
        <div class="container p-2">
            <a href="{{ route('bahagian.create')}}"
                class="btn btn-success"
            > Add New Bahagian </a>
        </div>
        <table class="table table-striped table-bordered table-sm">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            @foreach($bahagians as $bahagian)
                @if ($loop->first)
                    <tr>
                        <td colspan='4'> FIRST ROW </td>
                    </tr>
                @endif
                <tr>
                    <td>{{ $bahagian->id }}</td>
                    <td>{{ $bahagian->kod }}</td>
                    <td>{{ $bahagian->nama }}</td>
                    <td>
                        <a href="{{ route('bahagian.show', $bahagian)}}"
                            class="btn btn-success"
                        > View </a>
                        <a href="{{ route('bahagian.edit', $bahagian)}}"
                            class="btn btn-success"
                        > Edit </a>
                        <form action="{{ route('bahagian.destroy', $bahagian)}}"
                            method="POST" style="float:right">
                            @csrf
                            @method('delete')
                         <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @if ($loop->last)
                    <tr>
                        <td colspan='4'> LAST ROW </td>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>
</x-app-layout>
