<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bahagian') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <form method="POST" action="{{ route('bahagian.store') }}">
                    @csrf

                    <!-- Kod -->
                    <div class="mb-3">
                        <label for="kod" class="form-label">Kod</label>
                        <input type="text" class="form-control" id="kod"
                               name="kod" :value="old('kod')" required autofocus
                        >
                        @error('kod')
                        <div class="text-red">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Name -->
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama"
                            name="nama" :value="old('nama')" required autofocus
                        >
                        @error('nama')
                            <div class="text-red">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="pt-4">
                        <button class="btn btn-primary">
                            {{ __('Submit') }}
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
