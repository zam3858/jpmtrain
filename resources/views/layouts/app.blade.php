<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap"> -->

        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <link href="https://cdn.jsdelivr.net/npm/@coreui/coreui@4.0.2/dist/css/coreui.min.css" rel="stylesheet" integrity="sha384-qHZDARcDE4rvRsEXG23QUfqK+Pz9TDOdzRs1YuUyn4PfjMAJ9PbHcIZwg0Dx2yMP" crossorigin="anonymous">

        
    </head>
    <body>
        <div class="container-lg">
            @auth
                @include('layouts.navigation')
            @endauth
            <div class="container-lg">
            <!-- Page Heading -->
            <header class="header">
                    {{ $header }}
            </header>
        </div>

        <!-- Page Content -->
        <div class="container-lg">
            {{ $slot }}
        </div>

        @env('staging')
            <footer>
                THIS IS A STAGING SITE!!
            </footer>
        @endenv
        <script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@4.0.2/dist/js/coreui.bundle.min.js" integrity="sha384-mUCFKFW9jQEIjhS6xOIxqiVI8jvvVKP9EU7P7Hu9CXqwso926ZGo41SQRhUX8CdC" crossorigin="anonymous"></script>
    </body>
</html>
