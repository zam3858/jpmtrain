<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      CoreUI
    </a>
    <ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link active" href="{{ route('dashboard') }}">Dashboard</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('users.index') }}">Users</a>
  </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('bahagian.index') }}">Bahagians</a>
    </li>
  <li class="nav-item">
    <form method="POST" action="{{ route('logout') }}">
                            @csrf

        <a href="route('logout')" class="nav-link"
                onclick="event.preventDefault();
                            this.closest('form').submit();">
            {{ __('Log Out') }}
        </a>
    </form>
  </li>
</ul>
  </div>

</nav>


