<html>
<head></head>
<body>
    <table>
        <tr><td>Name</td><td>{{ $user->name }}</td></tr>
        <tr><td>Email</td><td>{{ $user->email }}</td></tr>
        <tr>
            <td>Bahagian</td>
            <td>
                <table class="table table-striped">
                    @foreach($user->bahagians as $bahagian)
                        <tr>
                            <td width="10px">{{ $loop->iteration }}</td>
                            <td>{{ $bahagian->nama }}</td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
