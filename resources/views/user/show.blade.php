<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Maklumat User') }}
        </h2>
    </x-slot>

    <div class="container">
        <table class="table table-sm table-hover">
            <tr>
                <td>Name</td>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td>Bahagian</td>
                <td>
                    <table class="table table-striped">
                        @foreach($user->bahagians as $bahagian)
                            <tr>
                                <td width="10px">{{ $loop->iteration }}</td>
                                <td>{{ $bahagian->nama }}</td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        </table>
    </div>
</x-app-layout>
