<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <form method="POST" action="{{ route('users.update', $user) }}">
                    @csrf
                    @method('PUT')

                    <!-- Name -->
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name"
                                   name="name" value="{{ old('name', $user->name) }}" required autofocus
                            >
                            @error('name')
                            <div class="text-red">{{ $message }}</div>
                            @enderror
                        </div>

                        <!-- Email Address -->
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email"
                                   name="email" value="{{ old('email', $user->email) }}" required
                            >
                            @error('email')
                            <div class="text-red">{{ $message }}</div>
                            @enderror
                        </div>

                    <div class="mt-4">
                        <label for="jabatan_id"> Bahagian </label>
                        <select
                            id="bahagian_id" class="form-control"
                            name="bahagian_id"
                        />
                            <option value="">Sila Pilih</option>
                            @foreach($senarai_bahagian as $index => $bahagian)
                                <option
                                    value="{{ $bahagian->id }}"
                                    @if($bahagian->id == old('bahagian_id', $user->bahagian_id)) selected @endif
                                >{{ $bahagian->nama }}</option>
                            @endforeach

                        </select>
                        @error('bahagian_id')
                            <div class="text-red">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="mt-4">
                        <label for="supervisor_id"> Supervisor </label>
                        <select
                            id="supervisor_id" class="form-control"
                            name="supervisor_id"
                        />
                            <option value="">Sila Pilih</option>
                            @foreach($senarai_supervisor as $index => $supervisor)
                                @if($supervisor->id != $user->id)
                                    <option value="{{ $supervisor->id }}">{{ $supervisor->name }}</option>
                                @else
                                    <option value="{{ $supervisor->id }}">Current User</option>
                                @endif
                            @endforeach
                        </select>
                        @error('supervisor_id')
                            <div class="text-red">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="pt-4">
                        <button class="btn btn-primary">
                            {{ __('Submit') }}
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
