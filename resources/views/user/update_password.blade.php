<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <form method="POST" action="{{ route('users.update_password_submit') }}">
                    @csrf
                    @method('PUT')

                    <!-- Password -->
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password"
                               name="password" :value="old('password')" required
                        >
                        @error('password')
                        <div class="text-red">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Confirm Password -->
                    <div class="mb-3">
                        <label for="password_confirmation" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password_confirmation"
                               name="password_confirmation" :value="old('password_confirmation')" required
                        >
                        @error('password_confirmation')
                        <div class="text-red">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="pt-4">
                        <button class="btn btn-primary">
                            {{ __('Submit') }}
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
