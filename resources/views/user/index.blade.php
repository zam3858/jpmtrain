<x-app-layout>
    <x-slot name="header">
        <span class="header-brand mb-0 h3">{{ __('Users') }}</span>
    </x-slot>

    <div>
        <div class="container p-2">
            <a href="{{ route('users.update_password')}}"
                class="btn btn-primary"
            > Update My Password </a>
            <a href="{{ route('users.create')}}"
                class="btn btn-success"
            > Add New User </a>
        </div>
        <table class="table table-striped table-bordered table-sm">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Mengetuai</th>
                <th>Bahagian</th>
                <th>Register date</th>
                <th></th>
            </tr>
            <tr>
                <td colspan="7">
                    <form action="">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="search"
                                       name="search" value="{{ $search ?? '' }}"
                                       placeholder="Search"
                                >
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="search2"
                                       name="search2" value="{{ $search2 ?? '' }}"
                                       placeholder="Search2"
                                >
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-primary m-2">Search</button>
                        </div>
                    </form>
                </td>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name_email }}</td>
                    <td>
                        @foreach($user->bahagians as $bahagian)
                            {{ $bahagian->nama }},
                        @endforeach
                    </td>
                    <td>
                        {{ $user->bahagian->nama ?? 'N/A' }}
                    </td>
                    <td>{{  $user->created_at->format('M Y') }}</td>
                    <td>
                        <a href="{{ route('users.show', $user)}}"
                            class="btn btn-success"
                        > View </a>
                        <a href="{{ route('users.edit', $user)}}"
                            class="btn btn-success"
                        > Edit </a>
                        <form action="{{ route('users.destroy', $user)}}"
                            method="POST" style="float:right">
                            @csrf
                            @method('delete')
                         <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $users->appends(request()->all())->links() }}
    </div>
</x-app-layout>
